#ifndef ADC_H
#define ADC_H
#include <ADS1220_WE.h>
#include <SPI.h>
//#include "FunctionalInterrupt.h" //ref: https://forum.arduino.cc/t/interrupt-in-class-esp32/1039326/11

/* Create your ADS1220 object */
#define HR_CS_PIN 5    // chip select pin
#define HR_DRDY_PIN 22 // data ready pin
#define EMG_CS_PIN 4    // chip select pin
#define EMG_DRDY_PIN 17 // data ready pin


class ADS1220
{
public:
    ADS1220(String name, uint8_t csPin, uint8_t drdyPin)
    {
        Serial.println("ADS1220 constructor()");
        this->csPin = csPin;
        this->drdyPin = drdyPin;
        this->name = name;
        ads = new ADS1220_WE(csPin, drdyPin);
        if (!ads->init())
        {
            Serial.println("ADS1220 is not connected!");
            while (1)
                ;
        }
        
    }

    //ref: https://forum.arduino.cc/t/a-class-and-an-interrupt-defined-within-it/352213/4
    void setupInterruptHandler(void (*ISR)(void)) {
        attachInterrupt(digitalPinToInterrupt(drdyPin), ISR, FALLING);
    }

    void init()
    {
        Serial.println("ADS1220 init()");
        Serial.println("CS pin =" + String(this->csPin) + " DRDY: " + String(this->drdyPin));

        /* The data rate level with setDataRate(). The data rate itself also depends on the operating
            mode and the clock. If the internal clock is used or an external clock with 4.096 MHz the data
            rates are as follows (per second):

            Level               Normal Mode      Duty-Cycle      Turbo Mode
            ADS1220_DR_LVL_0          20               5               40         (default)
            ADS1220_DR_LVL_1          45              11.25            90
            ADS1220_DR_LVL_2          90              22.5            180
            ADS1220_DR_LVL_3         175              44              350
            ADS1220_DR_LVL_4         330*             82.5            660
            ADS1220_DR_LVL_5         600             150             1200
            ADS1220_DR_LVL_6        1000             250             2000

            The higher the data rate, the higher the noise (tables are provided in section 7.1 in the
            data sheet). In single-shot mode the conversion times equal the times in Normal Mode.
        */
        // Settings from test run using ADS1220_basic_example code
        ads->bypassPGA(true);
        ads->setGain(ADS1220_GAIN_1);
        ads->setCompareChannels(ADS1220_MUX_0_1);
        //Test:
        //ads->setCompareChannels(ADS1220_MUX_0_AVSS);
        ads->setOperatingMode(ADS1220_NORMAL_MODE);
        ads->setDataRate(ADS1220_DR_LVL_4);
        ads->setFIRFilter(ADS1220_50HZ_60HZ);
        ads->setConversionMode(ADS1220_CONTINUOUS);
        Serial.println("GAIN = " + String(ads->getGainFactor()));
    }
    /* IRAM_ATTR was removed for linker to work when using a class method for ISR */
    void dataReadyIsr()
    {
        portENTER_CRITICAL(&mutex);
        newAdcDataAvailable = true;
        portEXIT_CRITICAL(&mutex);
        lastSampleTimeStampMills = millis();
    }

    bool isDataAvailable()
    {
        //Check if last sample was read with in MAX_ADC_SAMPLE_TIMEOUT_MILLS
        if (millis() - lastSampleTimeStampMills > MAX_ADC_SAMPLE_TIMEOUT_MILLS)
        {
            ads->reset();
            init();
        }
        return newAdcDataAvailable;
    }

    long getReading()
    {
        long results = ads->getRawData();
        portENTER_CRITICAL(&mutex);        
        newAdcDataAvailable = false;
        portEXIT_CRITICAL(&mutex);        
        lastAdcReading = results;

        return results;
    }

    long getLastReading() const
    {
        return lastAdcReading;
    }

private:
    ADS1220_WE *ads;
    String name;
    long lastAdcReading;
    uint8_t csPin;
    uint8_t drdyPin;
    volatile bool newAdcDataAvailable;
    portMUX_TYPE mutex = portMUX_INITIALIZER_UNLOCKED;
    unsigned long lastSampleTimeStampMills=0;
    const unsigned int MAX_ADC_SAMPLE_TIMEOUT_MILLS = 100;

};

#endif /* ADC_H */