#define TEST_SIG_PIN 16      

static unsigned long lastToggled = 0;

void testSigInit()
{
    pinMode(TEST_SIG_PIN, OUTPUT);
}

void generateTestSignal(unsigned int frequency)
{
    // Frequency should be in Hz
    // Calculating the period in microseconds
    unsigned long period = (1.0 / frequency) * 1000000;

    // Checking if it's time to toggle the state
    if (micros() - lastToggled > period/2)
    {
        // Toggling the state of the pin
        digitalWrite(TEST_SIG_PIN, !digitalRead(TEST_SIG_PIN));
        
        // Recording the current time as the last time the pin was toggled
        lastToggled = micros();
    }
}