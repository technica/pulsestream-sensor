#ifndef WIFI_SERVER_H
#define WIFI_SERVER_H

#include <ESPmDNS.h>
#include <WebServer.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include "../openBciApi/openBciApi.h"

#define SOFT_AP_SSID "OpenBCI WiFi AP"
#define SOFT_AP_PASSWORD "openbci1234"

#define JSON_BUFFER_SIZE 1024
extern size_t wifi_latency;
extern WebServer web_server;
extern WiFiClient tcp_client;

extern IPAddress local_ip;
extern IPAddress network_gateway;
extern IPAddress subnet_mask;
extern bool streaming_enabled;

void get_system_info();
void get_board_info();
void process_command();
void start_streaming();
void stop_streaming();
void switch_raw_output();
void get_tcp_config();
void set_tcp_config();
void stop_tcp_connection();
void invalid_request();
void webServerInit();

#endif // WIFI_SERVER_H
