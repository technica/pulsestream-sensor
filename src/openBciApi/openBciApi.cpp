#include <Arduino.h>
#include "openBciApi.h"

openbci_data_packet openbci_data_buffer[OPENBCI_DATA_BUFFER_SIZE];
openbci_data_packet tcp_transfer_buffer[OPENBCI_DATA_BUFFER_SIZE];
bool streaming_enabled = false;
uint64_t last_micros = 0;
uint16_t openbci_data_buffer_head = 0;
uint16_t openbci_data_buffer_tail = 0;

uint8_t channel_setting_buffer[8] = {0};

const int SAMPLING_RATE = 250;
const long SAMPLES_PER_MILLISEC = 4;
const long SAMPLES_PER_MICROSEC = 4000;

extern long get_last_adc_reading();
extern ADS1220 adc;

//Copies the ADC cached data to the OpenBCI buffer
void copyAdcDataToOpenbciBuffer(long adcHeartBeatReading, long adcEmgReading)
{
    static uint8_t sample_counter = 0;

    // Assuming the 'channel_data' needs to be zeroed out initially.
    uint8_t channel_data[24] = {0};

    // Store the adcHeartBeat data into channel_data[0-2].
    channel_data[0] = (uint8_t)((adcHeartBeatReading >> 16) & 0xFF);
    channel_data[1] = (uint8_t)((adcHeartBeatReading >> 8) & 0xFF);
    channel_data[2] = (uint8_t)(adcHeartBeatReading & 0xFF);

    // Store the adcEmg data into channel_data[3-5].
    channel_data[3] = (uint8_t)((adcEmgReading >> 16) & 0xFF); // Most significant byte (MSB)
    channel_data[4] = (uint8_t)((adcEmgReading >> 8) & 0xFF);  // Middle byte
    channel_data[5] = (uint8_t)(adcEmgReading & 0xFF);         // Least significant byte (LSB)

    if (streaming_enabled)
    {
        openbci_data_buffer[openbci_data_buffer_tail].header = 0xA0;
        openbci_data_buffer[openbci_data_buffer_tail].sample_number = sample_counter++;

        memcpy(&openbci_data_buffer[openbci_data_buffer_tail].channel_data, channel_data, sizeof(openbci_data_buffer[0].channel_data));

        memset(&openbci_data_buffer[openbci_data_buffer_tail].auxiliary_data, 0x00, sizeof(openbci_data_buffer[openbci_data_buffer_tail].auxiliary_data));

        openbci_data_buffer[openbci_data_buffer_tail].footer = 0xC0;

        if (++openbci_data_buffer_tail >= 50)
            openbci_data_buffer_tail = 0;
    }
}

void openBciLoop(ADS1220 *adcHeartBeat, ADS1220 *adcEmg)
{
    //if (streaming_enabled == true)
    {
        uint64_t current_micros = micros();

        if (adcHeartBeat->isDataAvailable() || adcEmg->isDataAvailable())
        {
            //Read ADC and cache it
            if (adcHeartBeat->isDataAvailable())
            {
                adcHeartBeat->getReading();
            }
            if (adcEmg->isDataAvailable())
            {
                adcEmg->getReading();
            }
            //Print the data heart rate and emg to serial
            Serial.println(String(adcHeartBeat->getLastReading()) + " " + String(adcEmg->getLastReading()));
            //Send data from the cached readings of ADCs    
            copyAdcDataToOpenbciBuffer(adcHeartBeat->getLastReading(), adcEmg->getLastReading());

            size_t tcp_write_size = wifi_latency / get_sample_delay();
            int16_t packets_to_write = openbci_data_buffer_tail - openbci_data_buffer_head;
            // If the buffer is wrapped around, adjust the packet count
            if (packets_to_write < 0)
            {
                packets_to_write += OPENBCI_DATA_BUFFER_SIZE;
            }

            //Start transferring data over TCP to OPENBCI UI
            if ((last_micros + wifi_latency <= current_micros) || (packets_to_write >= tcp_write_size))
            {

                // Copy the data to a buffer to send over TCP
                if (openbci_data_buffer_head + packets_to_write >= OPENBCI_DATA_BUFFER_SIZE)
                {
                    size_t wrap_size = OPENBCI_DATA_BUFFER_SIZE - openbci_data_buffer_head;
                    // Copy the wrapped data to the transfer buffer
                    memcpy(tcp_transfer_buffer, &openbci_data_buffer[openbci_data_buffer_head], wrap_size * sizeof(openbci_data_packet));
                    // Copy the rest of the data to the transfer buffer
                    memcpy(tcp_transfer_buffer + (wrap_size * sizeof(openbci_data_packet)), &openbci_data_buffer, (packets_to_write - wrap_size) * sizeof(openbci_data_packet));
                }
                else
                {
                    // Copy the data to the transfer buffer, no wrap around case
                    memcpy(tcp_transfer_buffer, &openbci_data_buffer[openbci_data_buffer_head], packets_to_write * sizeof(openbci_data_packet));
                }
                // Send the data over TCP
                tcp_client.write((uint8_t *)tcp_transfer_buffer, packets_to_write * sizeof(openbci_data_packet));
                // Update the buffer head pointer
                openbci_data_buffer_head = openbci_data_buffer_tail;

                last_micros = current_micros;
            }
        }

        //Samples are sent
    }
   
}
