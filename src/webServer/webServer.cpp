#include <Arduino.h>
#include "webServer.h"

WiFiClient tcp_client;
WebServer web_server(80);
size_t wifi_latency = 0;
IPAddress local_ip(192, 168, 4, 1);
IPAddress network_gateway(192, 168, 4, 1);
IPAddress subnet_mask(255, 255, 255, 0);

size_t get_sampling_rate()
{
    return 250; // Dummy sampling rate
}

size_t get_sample_delay()
{
    return 1000000 / get_sampling_rate();
}

size_t gain_from_channel(uint8_t channel_index)
{
    uint8_t gain = 0b010; // Dummy gain

    switch (gain)
    {
    case 0b000:
        return 1;
    case 0b001:
        return 2;
    case 0b010:
        return 4;
    case 0b011:
        return 6;
    case 0b100:
        return 8;
    case 0b101:
        return 12;
    case 0b110:
        return 24;
    default:
        return 0;
    }
}

IPAddress ip_from_string(String ip_string)
{
    IPAddress ip_address(0, 0, 0, 0);

    ip_address.fromString(ip_string);

    return ip_address;
}

uint8_t digit_from_char(char digit_char)
{
    return digit_char - '0';
}

void get_system_info()
{
    Serial.println("get_system_info()");
    DynamicJsonDocument json_document(JSON_BUFFER_SIZE);

    JsonObject json_object = json_document.to<JsonObject>();

    json_object["board_connected"] = true;
    json_object["heap"] = ESP.getFreeHeap();
    json_object["ip"] = WiFi.softAPIP().toString();
    json_object["latency"] = wifi_latency;
    json_object["mac"] = WiFi.softAPmacAddress();
    json_object["name"] = OPENBCI_NAME;
    json_object["num_channels"] = 8;
    json_object["version"] = OPENBCI_VERSION;

    String json_string = "";

    serializeJson(json_document, json_string);

    web_server.send(200, "text/json", json_string);
}

void get_board_info()
{
    Serial.println("get_board_info()");
    DynamicJsonDocument json_document(JSON_BUFFER_SIZE);

    JsonObject json_object = json_document.to<JsonObject>();

    json_object["board_connected"] = true;
    json_object["board_type"] = "cyton";

    JsonArray gains = json_object.createNestedArray("gains");

    for (size_t channel_index = 0; channel_index < 8; channel_index++)
        gains.add(gain_from_channel(channel_index));

    json_object["num_channels"] = 8;

    String json_string = "";

    serializeJson(json_document, json_string);

    web_server.send(200, "text/json", json_string);
}

void process_command()
{
    Serial.println("process_command();");
    DynamicJsonDocument json_document(JSON_BUFFER_SIZE);

    deserializeJson(json_document, web_server.arg(0));

    JsonObject json_object = json_document.as<JsonObject>();

    String command = json_object["command"];

    String return_message = "";

    bool streaming_state = streaming_enabled;

    streaming_enabled = false;

    Serial.println("Command : " + String(command));

    delayMicroseconds(50);

    if (command[0] == '~')
    {
        Serial.println("Action  : Set SAMPLING RATE " + String(digit_from_char(command[1])));

        return_message = "Success: Sample rate is now ";
        return_message += get_sampling_rate();
        return_message += "Hz";
    }
    else if (command == "1")
    {
        Serial.println("Setting channel 1");
    }
    else if (command == "2")
    {
        Serial.println("Setting channel 2");
    }
    else if (command == "3")
    {
        Serial.println("Setting channel 3");
    }
    else if (command == "4")
    {
        Serial.println("Setting channel 4");
    }
    else if (command == "5")
    {
        Serial.println("Setting channel 5");
    }
    else if (command == "6")
    {
        Serial.println("Setting channel 6");
    }
    else if (command == "7")
    {
        Serial.println("Setting channel 7");
    }
    else if (command == "8")
    {
        Serial.println("Setting channel 8");
    }
    else if (command == "!")
    {
        Serial.println("Restoring channel 1 settings");
    }
    else if (command == "@")
    {
        Serial.println("Restoring channel 2 settings");
    }
    else if (command == "#")
    {
        Serial.println("Restoring channel 3 settings");
    }
    else if (command == "$")
    {
        Serial.println("Restoring channel 4 settings");
    }
    else if (command[0] == 'x')
    {
        Serial.println("Command is x");
    }
    else if (command == "b")
    {
        Serial.println("Start streaming");
        streaming_state = true;
    }

    else if (command == "s")
    {
        Serial.println("Start streaming");
        streaming_state = false;
    }

    streaming_enabled = streaming_state;

    web_server.send(200, "text/plain", return_message);
}

void start_streaming()
{
    Serial.println("start_streaming()");
    streaming_enabled = true;

    web_server.send(200);
}

void stop_streaming()
{
    Serial.println("stop_streaming()");
    streaming_enabled = false;

    web_server.send(200);
}

void switch_raw_output()
{
    Serial.println("switch_raw_output()");
    web_server.send(200, "text/plain", "Output mode configured to raw");
}

void get_tcp_config()
{
    Serial.println("get_tcp_config()");
    DynamicJsonDocument json_document(JSON_BUFFER_SIZE);

    JsonObject json_object = json_document.to<JsonObject>();

    json_object["connected"] = (tcp_client.connected() != 0) ? true : false;
    json_object["delimiter"] = false;
    json_object["ip_address"] = tcp_client.remoteIP().toString();
    json_object["output"] = "raw";
    json_object["port"] = tcp_client.remotePort();
    json_object["latency"] = wifi_latency;

    String json_string = "";

    serializeJson(json_document, json_string);

    web_server.send(200, "text/json", json_string);
}

void set_tcp_config()
{
    Serial.println("set_tcp_config()");
    streaming_enabled = false;

    DynamicJsonDocument json_document(JSON_BUFFER_SIZE);

    deserializeJson(json_document, web_server.arg(0));

    JsonObject json_object = json_document.as<JsonObject>();

    String tcp_client_ip = json_object["ip"];
    wifi_latency = json_object["latency"];
    uint16_t tcp_client_port = json_object["port"];

    tcp_client.stop();

    tcp_client.connect(ip_from_string(tcp_client_ip), tcp_client_port);

    tcp_client.setNoDelay(1);

    get_tcp_config();
}

void stop_tcp_connection()
{
    Serial.println("stop_tcp_connection()");
    streaming_enabled = false;

    tcp_client.stop();

    get_tcp_config();
}

void invalid_request()
{
    Serial.println("invalid_request()");
    web_server.send(404, "text/plain", "Invalid Request!");
}

void webServerInit() {
    delayMicroseconds(50);

    WiFi.mode(WIFI_AP);
    WiFi.softAP(SOFT_AP_SSID, SOFT_AP_PASSWORD);

    delay(250);

    WiFi.softAPConfig(local_ip, network_gateway, subnet_mask);

    delay(250);    
    Serial.println("Starting OpenBCI AP");
    MDNS.begin("openbci");

    // tcp_transfer_buffer = (uint8_t *)malloc(sizeof(openbci_data_buffer));
    // Serial.println("tcp_transfer_buffer");
    // Serial.println(long(&tcp_transfer_buffer));

    web_server.on("/all", HTTP_GET, get_system_info);
    web_server.on("/board", HTTP_GET, get_board_info);

    web_server.on("/command", HTTP_POST, process_command);

    web_server.on("/stream/start", HTTP_GET, start_streaming);
    web_server.on("/stream/stop", HTTP_GET, stop_streaming);

    web_server.on("/output/raw", HTTP_GET, switch_raw_output);

    web_server.on("/tcp", HTTP_GET, get_tcp_config);
    web_server.on("/tcp", HTTP_POST, set_tcp_config);
    web_server.on("/tcp", HTTP_DELETE, stop_tcp_connection);

    web_server.onNotFound(invalid_request);

    MDNS.addService("http", "tcp", 80);

    web_server.begin();
}