/*

PulseStream EMG/Heartrate signal collection peripheral
Copyright (C) <2024> <Sachin Surendran>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

*/
#include <math.h>
#include "src/debug/debugLed.h"
#include "src/openBciApi/openBciApi.h"
#include "src/webServer/webServer.h"
#include "src/testSig/testSig.h"

#define CORE_1 1
#define CORE_0 0

ADS1220 adcHeartBeat("HeartRate", HR_CS_PIN, HR_DRDY_PIN);
void IRAM_ATTR dataReadyISRHeartRate()
{
    adcHeartBeat.dataReadyIsr();
}


ADS1220 adcEmg("EMG", EMG_CS_PIN, EMG_DRDY_PIN);
void IRAM_ATTR dataReadyISREmg()
{
    adcEmg.dataReadyIsr();
}

// Use to get ADC data over Serial monitor
bool adcTestMode = false;

void setup()
{
    Serial.begin(115200);

    debugLedInit();
    testSigInit();
    adcHeartBeat.setupInterruptHandler(dataReadyISRHeartRate);
    adcHeartBeat.init();
    adcEmg.setupInterruptHandler(dataReadyISREmg);
    adcEmg.init();
    if (!adcTestMode)    
    {
        webServerInit();
    }
}



unsigned long lastMillisEmg = 0; // Stores the last time we updated the sample rate
unsigned int sampleCountEmg = 0; // Counts how many samples we have taken
unsigned long lastMillisHeartRate = 0; // Stores the last time we updated the sample rate
unsigned int sampleCountHeartRate = 0; // Counts how many samples we have taken
long adcEmgReading = 0;
long adcHeartBeatReading = 0;




void loop()
{
    if (adcTestMode)
    {
        bool newAdcDataAvailable = false;
        
        if (adcEmg.isDataAvailable())
        {
            adcEmgReading= adcEmg.getReading();
            //Serial.println(String(adcEmgReading));
            sampleCountEmg++;
            newAdcDataAvailable = true;
        }
        
        if (adcHeartBeat.isDataAvailable())
        {
            adcHeartBeatReading= adcHeartBeat.getReading();
            sampleCountHeartRate++;
            newAdcDataAvailable = true;
        }
        //If there is new data available then print it
        if (newAdcDataAvailable) {
            //Serial.println(String(millis())+","+String(adcEmgReading)+","+String(adcHeartBeatReading));
            //Serial.println(String(millis())+" "+String(adcHeartBeatReading));
            Serial.println(String(adcEmgReading)+" "+String(adcHeartBeatReading));
        }


        //checkAndPrintSamplesPerSecond("EMG", lastMillisEmg, sampleCountEmg, lastSampleEmg);
        //checkAndPrintSamplesPerSecond("HEART_RATE", lastMillisHeartRate, sampleCountHeartRate, lastSampleHeartRate);
        
        //generateTestSignal(15);
    }
    else
    {     

        openBciLoop(&adcHeartBeat, &adcEmg);     
        //generateTestSignal(100);
        web_server.handleClient();
    }
}



inline void checkAndPrintSamplesPerSecond(String sensorName, unsigned long& lastMillis, unsigned int& sampleCount, unsigned int& lastSampleCount) {
    unsigned long currentMillis = millis();
    
    // If one second has passed
    if (currentMillis - lastMillis >= 1000) {
        // Print the number of samples per second
        Serial.print(sensorName+" samples per second: ");
        Serial.println(sampleCount);

        // Reset the sample count
        sampleCount = 0;
        // Reset the last sample count as well
        lastSampleCount = 0;

        // Save the current time for the next iteration
        lastMillis = currentMillis;
    }
}

