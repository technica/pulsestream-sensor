#ifndef OPENBCI_API_H
#define OPENBCI_API_H

#include <stdint.h>
#include <WiFi.h>
#include "../adc/adc.h"

#define OPENBCI_DATA_BUFFER_SIZE 50
#define OPENBCI_NAME "OpenBCI-FFFF"
#define OPENBCI_VERSION "v2.0.5"

typedef struct openbci_data_packet
{
    uint8_t header;

    uint8_t sample_number;

    uint8_t channel_data[24];

    uint8_t auxiliary_data[6];

    uint8_t footer;
} __attribute__((packed)) openbci_data_packet;

extern size_t wifi_latency;
extern size_t get_sample_delay();

extern WiFiClient tcp_client;

void openBciLoop(ADS1220 *adcHeartBeat, ADS1220 *adcEmg);


#endif // OPENBCI_API_H
