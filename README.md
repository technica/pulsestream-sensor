# pulsestream-sensor

PulseStream, a project dedicated to accessible gathering of crucial biometric data. Utilizing ADC (Analog-to-Digital Converter) hardware, PulseStream captures health metrics like heart rate and EMG (Electromyography) signals. 

This hardware collects the data and exports it via Wifi and USB for viewing and analysis.

## Basic Architecture:
![Architecture Image](img/pulsestream-architecture.png)

### Signal view of Vertical Eye Muscle movement:

![Architecture Image](img/EMG_eye_open_close.png)

### Signal view of Hand clasping movement:

![Architecture Image](img/EMG_hand_clasp.png)

### Signal view of Heart Rate signal collected from Pulse Sensor:

![Architecture Image](img/HeartRate.png)

### PCB of PulseStream board

![Architecture Image](img/pulseStreamPcb_3dView.png)


