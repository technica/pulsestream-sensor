constexpr int DEBUG_LED = 2; //Devkit v1 onboard led
void debugLedInit()
{
    pinMode(DEBUG_LED, OUTPUT);
}

void flashDebugLed(int times)
{
    for (int i = 0; i < times; i++)
    {
        digitalWrite(DEBUG_LED, HIGH);
        delay(500);
        digitalWrite(DEBUG_LED, LOW);
        delay(500);
    }
}